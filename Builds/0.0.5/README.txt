GAM111_3_Prototype v0.0.5 build 1

===CONTROLLS===
Move		-	WASD
Jump		-	Space
Sprint		-	Shift
Interact/Pickup	-	E
Shoot		-	LMB



===CHANGELOG===
Added NPC
Added Blue Key

Removed bullet impact effect


===PUZZLE SOLUTION===
Grab the Orange Key to start the elevator
Press the button to activate the portal
Go through the portal and use a cube to grab the Blue Key
Go back up the elevator onto the blue platform