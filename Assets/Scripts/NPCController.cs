﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class NPCController : MonoBehaviour
{
    NavMeshAgent agent;
    public float stoppingDistance;
    public GameObject player;

    // Use this for initialization
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(this.transform.position, player.transform.position) <= stoppingDistance)
        {
            return;
        }
        else
        {
            agent.SetDestination(player.transform.position);
        }
    }
}
