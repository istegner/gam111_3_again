﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Items that can go in the players invent
/// </summary>
[CreateAssetMenu()]
public class ItemSO : ScriptableObject
{
}
