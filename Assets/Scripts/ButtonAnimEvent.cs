﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonAnimEvent : MonoBehaviour
{
    GameObject gameManager;
    AnimationController animationController;
    public bool elevator;
    public bool portal;

    private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
        animationController = gameManager.GetComponent<AnimationController>();
        //If one button controlls a portal and elevator, warn the user
        if (elevator && portal)
        {
            Debug.LogWarning("Button controls both portal and elevator");
        }
    }

    /// <summary>
    /// Function to be run at the end of the button push
    /// Disable the buttons animator
    /// </summary>
    public void ButtonPushEnd()
    {
        gameObject.GetComponent<Animator>().enabled = false;
        animationController.buttonPushed = false;
    }

    /// <summary>
    /// Start the elevator and portal
    /// </summary>
    public void GoTime()
    {
        if(portal && elevator)
        {
            PortalTime();
            ElevatorTime();
        }
        else if (portal)
        {
            PortalTime();
        }
        else if (elevator)
        {
            ElevatorTime();
        }
    }

    /// <summary>
    /// Make the portal go up
    /// </summary>
    void PortalTime()
    {
        animationController.DoPortal();
    }

    /// <summary>
    /// Make the elevator elevate
    /// </summary>
    void ElevatorTime()
    {
        animationController.DoElevator();
    }
}
