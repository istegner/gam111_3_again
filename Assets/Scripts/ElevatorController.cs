﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class ElevatorController : MonoBehaviour
{
    public GameObject player;
    InventoryController playerInvent;
    public ItemSO key;

    public Animator elevatorAnim;
    bool isElevatoring = false;

    void Start()
    {
        elevatorAnim.enabled = false;
        playerInvent = player.GetComponent<InventoryController>();
    }

    /// <summary>
    /// If the player has the right key
    /// make the elevator go
    /// </summary>
    private void Update()
    {
        if (!isElevatoring)
        {
            if (playerInvent.inventorySO.HasItem(key))
            {
                isElevatoring = true;
                DoElevator();
            }
        }
    }

    /// <summary>
    /// Keep the elevator going
    /// </summary>
    public void DoElevator()
    {
        elevatorAnim.enabled = true;
    }

    public void Ding()
    {
        //AudioSource.Instantiate()
    }
}
