﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeController : MonoBehaviour
{
    //How long should the effect last
    public float spawnEffectTime = 2;
    //How long to wait after effect is finished
    public float pause = 1;
    public AnimationCurve fadeIn;

    ParticleSystem ps;
    float timer = 0;
    Renderer _renderer;

    int shaderProperty;

    //The cubes health
    public float health;
    //The death cube prefab
    public GameObject deathCube;
    //The normal material of the cube after the spawn effect
    public Material normalMat;

    //The spawn point of the cube
    public Transform spawnPoint;
    //The cubes original parent
    public Transform parent;

    /// <summary>
    /// When the cube is enabled, set it transform to its spawn
    /// Allow it to be collided with
    /// Play the spawn in effect
    /// </summary>
    void OnEnable()
    {
        gameObject.transform.position = spawnPoint.position;
        gameObject.transform.rotation = spawnPoint.rotation;
        gameObject.GetComponent<BoxCollider>().isTrigger = false;

        shaderProperty = Shader.PropertyToID("_cutoff");
        _renderer = GetComponent<Renderer>();
        ps = GetComponentInChildren<ParticleSystem>();

        var main = ps.main;
        main.duration = spawnEffectTime;

        ps.Play();

    }

    void Update()
    {
        if (timer < spawnEffectTime + pause)
        {
            timer += Time.deltaTime;
        }
        else
        {
            _renderer.material = normalMat;
        }

        _renderer.material.SetFloat(shaderProperty, fadeIn.Evaluate(Mathf.InverseLerp(0, spawnEffectTime, timer)));

    }

    /// <summary>
    /// Damage the cube
    /// If it has no health left
    /// kill it
    /// </summary>
    /// <param name="damage"></param>
    public void TakeDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Die();
        }
    }

    /// <summary>
    /// Make in cube uninteractable
    /// Create a death cube where it was
    /// Disable its game object
    /// </summary>
    void Die()
    {
        gameObject.GetComponent<BoxCollider>().isTrigger = true;
        Instantiate(deathCube, transform.position, transform.rotation);
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if(player.GetComponent<PlayerController>().pickedUpObject == this.gameObject)
        {
            player.GetComponent<PlayerController>().Drop();
        }
        gameObject.SetActive(false);
    }

    /// <summary>
    /// If the cube leaves the map
    /// kill it
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("OutOfBounds"))
        {
            Die();
        }
    }

    /// <summary>
    /// If the cube is on the elevator
    /// set it to its parent
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Elevator")
        {
            parent = this.gameObject.transform.parent;
            transform.parent = other.transform;
        }
    }

    /// <summary>
    /// If the cube leaves the elevator
    /// set reset its parent
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Elevator")
        {
            transform.parent = parent;
        }
    }
}
