﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FadeManager : MonoBehaviour
{
    //The black image to fade
    public Image black;
    //The animator that fades
    public Animator fadeAnim;

    /// <summary>
    /// Start fading out
    /// </summary>
    /// <param name="index"></param>
    public void StartFading(int index)
    {
        StartCoroutine(Fadeing(index));
    }

    /// <summary>
    /// Start the fade out animation
    /// Wait until fully faded
    /// If the scene has a loading screen
    /// load into new scene with loading screen
    /// Else
    /// load into new scene
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    IEnumerator Fadeing(int index)
    {
        fadeAnim.SetBool("Fade", true);
        yield return new WaitUntil(() => black.color.a == 1);
        if(this.GetComponent<LoadingScreenController>() != null)
        {
            this.GetComponent<LoadingScreenController>().LoadingScreenGo(index);
        }
        else
        {
            SceneManager.LoadScene(index);
        }
    }
}
