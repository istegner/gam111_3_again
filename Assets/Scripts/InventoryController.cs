﻿using UnityEngine;

public class InventoryController : MonoBehaviour
{
    public InventorySO inventorySO;
    
    /// <summary>
    /// Clear the players invent on game start
    /// </summary>
    public void Start()
    {
        inventorySO.Clear();
    }
}
