﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public GameObject gameManager;

    //The spawn point if the player leaves the map
    public Transform spawnPoint;

    //The interact text
    public GameObject pushText;
    //How far away should it appear at
    public float seeDistance;

    [Header("Pickup")]
    public float pickedUpOffset;
    public GameObject pickedUpObject;
    //How fast should the picked up object lerp into position
    public float smoothSpeed;
    //Delay so the player doesn't instantly pick up the object again
    public float pickupDelay;

    [Header("Gun Stuff")]
    public float maxDistance;
    public float damage;
    public float fireRate;
    bool canFire = true;

    [Header("Invent stuff")]
    public InventoryController playerInvent;
    public ItemSO orangeKey;

    [Header("Elevator")]
    public ElevatorController elevatorController;
    public Animator elevatorAnim;

    private void Start()
    {
        //Disable the curser and lock it to the center of the screen
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        playerInvent = GetComponent<InventoryController>();
        //Load the player settings from file
        LoadSettings();
    }
    
    void Update()
    {
        //Drop the picked up object if the player is not looking at it
        if (Input.GetKeyDown(KeyCode.E) && pickedUpObject != null)
        {
            Drop();
        }

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, seeDistance))
        {
            //If the player is close enough, show the interact text
            if (hit.transform.gameObject.CompareTag("Button"))
            {
                pushText.SetActive(true);
                //If the player presses 'E', make the button push
                if (Input.GetKeyDown(KeyCode.E))
                {
                    gameManager.GetComponent<AnimationController>().ButtonPush(hit.transform.gameObject);
                }
            }
            //If the player looks away, disable the interact text
            else
            {
                pushText.SetActive(false);
            }

            //If the player is close enough and looking at a pickup-able object, pick it up/drop it
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (hit.transform.gameObject.CompareTag("Grabbable"))
                {
                    if (pickedUpObject == null)
                    {
                        Pickup(hit.transform.gameObject);
                    }
                    else
                    {
                        Drop();
                    }
                }
            }
        }

        //If the player has picked up an object, carry it
        if (pickedUpObject != null)
        {
            Carry();
        }

        //Shoot when LMB is pressed
        RaycastHit gunHit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out gunHit, maxDistance))
        {
            if (Input.GetMouseButtonDown(0) && canFire)
            {
                Shoot(gunHit.transform);
            }
        }
    }

    /// <summary>
    /// If the player can pick up something
    /// pick it up and disable gravity on it
    /// </summary>
    /// <param name="toPickup"></param>
    void Pickup(GameObject toPickup)
    {
        if (pickupDelay <= 0)
        {
            pickedUpObject = toPickup;
            pickedUpObject.GetComponent<Rigidbody>().useGravity = false;
        }
    }

    /// <summary>
    /// Re-enable gravity on the picked up object
    /// and delay the next pickup
    /// </summary>
    public void Drop()
    {
        pickedUpObject.GetComponent<Rigidbody>().useGravity = true;
        pickedUpObject = null;
        pickupDelay = 1;
        StartCoroutine(DelayPickup());
    }

    /// <summary>
    /// Smoothly move the picked up objects position to the offset (in front of the player)
    /// </summary>
    void Carry()
    {
        var oldPos = pickedUpObject.transform.position;
        var newPos = Camera.main.transform.position + Camera.main.transform.forward * pickedUpOffset;
        pickedUpObject.transform.position = Vector3.Lerp(oldPos, newPos, smoothSpeed * Time.deltaTime);
    }

    /// <summary>
    /// Create an impact effect at the shot point which is destroyed after 10 seconds
    /// If the player shot a cube, make it take damage
    /// Delay the next shot
    /// </summary>
    /// <param name="hitPoint"></param>
    void Shoot(Transform hitPoint)
    {
        canFire = false;
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, maxDistance))
        {
            if (hit.transform.gameObject.CompareTag("Grabbable"))
            {
                hit.transform.gameObject.GetComponent<CubeController>().TakeDamage(damage);
            }
        }
        StartCoroutine(ShootDelay());
    }

    /// <summary>
    /// Delay when the player can shoot again
    /// </summary>
    /// <returns></returns>
    IEnumerator ShootDelay()
    {
        yield return new WaitForSeconds(fireRate);
        canFire = true;
    }

    /// <summary>
    /// Delay when the player can pick up an object
    /// </summary>
    /// <returns></returns>
    IEnumerator DelayPickup()
    {
        yield return new WaitForSeconds(1);
        pickupDelay--;
    }

    /// <summary>
    /// If the player leaved the map, respawn them
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("OutOfBounds"))
        {
            Respawn();
        }
    }

    /// <summary>
    /// Send the player back to their spawn point
    /// </summary>
    void Respawn()
    {
        if(pickedUpObject != null)
            Drop();
        gameObject.transform.position = spawnPoint.transform.position;
        gameObject.transform.rotation = spawnPoint.transform.rotation;
    }

    /// <summary>
    /// Load the player settings from file
    /// </summary>
    void LoadSettings()
    {
        if (File.Exists(Application.persistentDataPath + "/GameSettings.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/GameSettings.dat", FileMode.Open);
            GameSettings data = (GameSettings)bf.Deserialize(file);
            file.Close();

            GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_UseHeadBob = data.headBob;
            GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.XSensitivity = data.horiSensitivity;
            GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.YSensitivity = data.vertSensitivity;
        }
    }
}
