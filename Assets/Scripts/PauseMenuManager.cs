﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

public class PauseMenuManager : MonoBehaviour
{
    [Header("Pause menu")]
    //The pause menu canvas
    public GameObject pauseMenu;
    public GameObject optionsMenu;
    //The main game canvas
    public GameObject playUI;
    //Is the game paused
    public bool paused;

    [Header("Options menu")]
    //The resolutions the player can choose from (differs from computer to computer)
    Resolution[] resolutions;
    //The dropdown for the resolutions
    public TMP_Dropdown resolutionDropdown;
    public TMP_Dropdown qualityDropdown;
    //The toggle for fullscreen
    public Toggle fullscreenToggle;
    //The toggle to toggle showing fps
    public Toggle showFpsToggle;
    //The advanced FPS counter
    public Toggle showAdvancedFPSToggle;
    public GameObject advancedFPS;

    [Header("PlayerOptions")]
    //Toggle for the player head bob
    public Toggle headBobToggle;
    //The players horizontal sensitivity
    public Slider horizontalSlider;
    //The players vertical sensitivity
    public Slider verticalSlider;
    //The text elements to display the player sensitivity as a number
    public TMP_Text horiValue;
    public TMP_Text vertValue;

    public GameObject player;

    void OnEnable()
    {
        //Get the screens available resolutions
        resolutions = Screen.resolutions;
        //Clear the resolution dropdown
        resolutionDropdown.ClearOptions();
        //Create a list of resolutions and put then into the dropdown
        List<string> options = new List<string>();
        int currentResIndex = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResIndex = i;
            }
        }
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResIndex;
        resolutionDropdown.RefreshShownValue();
        //Load the game settings from file and apply them
        LoadSettings();
        //qualityDropdown.RefreshShownValue();
        horizontalSlider.value = player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.XSensitivity;
        verticalSlider.value = player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.YSensitivity;
        headBobToggle.isOn = player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_UseHeadBob;
        fullscreenToggle.isOn = Screen.fullScreen;
        showFpsToggle.isOn = this.GetComponent<FPSCounter>().show;
    }

    void Update()
    {
        if (paused)
        {
            //Show the players sensitivity to 2 decimal places
            horiValue.text = horizontalSlider.value.ToString("F2");
            vertValue.text = verticalSlider.value.ToString("F2");
        }
        //If the escape key is pressed pause/resume the game
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (paused)
            {
                if (optionsMenu.activeInHierarchy)
                {
                    SaveSettings();
                    optionsMenu.SetActive(false);
                    pauseMenu.SetActive(true);
                    return;
                }
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    /// <summary>
    /// Unhide the curser
    /// Set the game to paused and time scale to 0
    /// Enable the pause menu
    /// Disable the main UI
    /// </summary>
    public void Pause()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        paused = true;
        Time.timeScale = 0f;
        pauseMenu.SetActive(true);
        playUI.SetActive(false);
    }

    /// <summary>
    /// Hide the curser
    /// Disable the pause menu
    /// Enable the main UI
    /// Set the time scale back to normal
    /// Unpause the game
    /// </summary>
    public void Resume()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        pauseMenu.SetActive(false);
        playUI.SetActive(true);
        Time.timeScale = 1f;
        paused = false;
    }

    /// <summary>
    /// Set the time scale back to normal
    /// Unhide the curser
    /// Go back to the main menu
    /// </summary>
    public void ExitBtn()
    {
        Time.timeScale = 1f;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        this.GetComponent<FadeManager>().StartFading(0);
    }

    /// <summary>
    /// Set the game quality based of the dropdown value
    /// </summary>
    /// <param name="qualityIndex"></param>
    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    /// <summary>
    /// Enable/disable fullscreen mode
    /// </summary>
    /// <param name="isFullscreen"></param>
    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }

    /// <summary>
    /// Set the resolution of the game window based on the dropdown
    /// </summary>
    /// <param name="resolutionIndex"></param>
    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    /// <summary>
    /// Enable/disable headbob
    /// </summary>
    /// <param name="headBob"></param>
    public void HeadBob(bool headBob)
    {
        player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_UseHeadBob = headBob;
    }

    /// <summary>
    /// Set the vertical sensitivity
    /// </summary>
    /// <param name="ySensitivity"></param>
    public void SetVertSensitivity(float ySensitivity)
    {
        player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.YSensitivity = ySensitivity;
    }

    /// <summary>
    /// Set the horizontal sensitivity
    /// </summary>
    /// <param name="xSensitivity"></param>
    public void SetHoriSensitivity(float xSensitivity)
    {
        player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.XSensitivity = xSensitivity;
    }

    public void ShowFPS(bool fpsToggle)
    {
        this.GetComponent<FPSCounter>().show = fpsToggle;
    }

    public void ShowAdvancedFPS(bool advancedFPSToggle)
    {
        advancedFPS.SetActive(advancedFPSToggle);
    }

    /// <summary>
    /// Save the game settings to file
    /// </summary>
    public void SaveSettings()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/GameSettings.dat");

        GameSettings data = new GameSettings();
        data.resolution = resolutionDropdown.value;
        data.fullscreen = Screen.fullScreen;
        data.showFPS = this.GetComponent<FPSCounter>().show;
        data.showAdvancedFPS = advancedFPS.activeInHierarchy;
        data.graphics = QualitySettings.GetQualityLevel();
        data.headBob = player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_UseHeadBob;
        data.horiSensitivity = player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.XSensitivity;
        data.vertSensitivity = player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.YSensitivity;

        bf.Serialize(file, data);
        file.Close();
    }

    /// <summary>
    /// Load the game settings from file
    /// </summary>
    public void LoadSettings()
    {
        Debug.Log("Loading settings...");
        if (File.Exists(Application.persistentDataPath + "/GameSettings.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/GameSettings.dat", FileMode.Open);
            GameSettings data = (GameSettings)bf.Deserialize(file);
            file.Close();

            resolutionDropdown.value = data.resolution;
            Screen.fullScreen = data.fullscreen;
            this.GetComponent<FPSCounter>().show = data.showFPS;
            advancedFPS.SetActive(data.showAdvancedFPS);

            QualitySettings.SetQualityLevel(data.graphics);
            player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_UseHeadBob = data.headBob;
            player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.XSensitivity = data.horiSensitivity;
            player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.YSensitivity = data.vertSensitivity;
        }
        Debug.Log("Done loading settings.");
    }
}
