﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreenController : MonoBehaviour
{
    //The cavas that hold the loading screen
    public GameObject loadingScreenCanvas;
    //The slider that'll show the loading progress
    public Slider progressBar;

    AsyncOperation async;

    /// <summary>
    /// If the current scene is the game start scene, load the main menu
    /// </summary>
    private void Start()
    {
        if(SceneManager.GetActiveScene().buildIndex == 0)
        {
            LoadingScreenGo(1);
        }
    }

    /// <summary>
    /// Start the loading process
    /// </summary>
    /// <param name="index"></param>
    public void LoadingScreenGo(int index)
    {
        StartCoroutine(LoadingScreen(index));
    }

    /// <summary>
    /// Activate the loading screen
    /// Load the required scene in async but don't allow it to activate
    /// While the scene hasn't loaded
    /// show it's progress on the loading bar
    /// If the scene has fully loaded
    /// Set the loading bar to 100%
    /// Activate the new scene
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    IEnumerator LoadingScreen(int index)
    {
        loadingScreenCanvas.SetActive(true);
        async = SceneManager.LoadSceneAsync(index);
        async.allowSceneActivation = false;

        while (!async.isDone)
        {
            progressBar.value = async.progress;
            if(async.progress == 0.9f)
            {
                progressBar.value = 1f;
                async.allowSceneActivation = true;
            }
            yield return null;
        }
    }
}
