﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathCube : MonoBehaviour
{
    //How long should the effect last
    public float spawnEffectTime = 2;
    public AnimationCurve fadeIn;

    ParticleSystem ps;
    float timer = 0;
    Renderer _renderer;

    int shaderProperty;

    //The cube and it's spawn point in question
    public GameObject cube;
    public Transform spawnPoint;

    void Start()
    {
        shaderProperty = Shader.PropertyToID("_cutoff");
        _renderer = GetComponent<Renderer>();
        ps = GetComponentInChildren<ParticleSystem>();

        var main = ps.main;
        main.duration = spawnEffectTime;

        ps.Play();

    }

    void Update()
    {
        if (timer < spawnEffectTime)
        {
            timer += Time.deltaTime;
        }
        else
        {
            StartCoroutine(WaitRespawn());
        }
        _renderer.material.SetFloat(shaderProperty, fadeIn.Evaluate(Mathf.InverseLerp(0, spawnEffectTime, timer)));
    }

    //void Respawn()
    //{
    //    Instantiate(cube, spawnPoint.position, spawnPoint.rotation);
    //}

    /// <summary>
    /// Wait 5 seconds then desroy itself
    /// </summary>
    /// <returns></returns>
    IEnumerator WaitRespawn()
    {
        yield return new WaitForSeconds(5);
        //Respawn();
        Destroy(this.gameObject);
    }
}
