﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : MonoBehaviour
{
    //The SO of the collectable item
    public ItemSO item;
    //How many is the player picking up
    public int number = 1;
    public bool isOneShot = true;

    public UnityEngine.Events.UnityEvent OnPickedUp;

    /// <summary>
    /// When the player runs into the item
    /// add it to the invent
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        //find out if they have inventorycontroller
        var invCont = other.GetComponent<InventoryController>();

        if (invCont != null)
        {
            invCont.inventorySO.AddItem(item);

            OnPickedUp.Invoke();

            if (isOneShot)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
