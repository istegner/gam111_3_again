﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    public InventorySO playerInvent;
    public ItemSO orangeKey;

    [Header("Portal")]
    public GameObject portal;
    [SerializeField]
    Animator portalAmim;
    public bool portalUp;

    [Header("Elevator")]
    public GameObject elevator;
    [SerializeField]
    Animator elevatorAnim;
    public bool elevatorGo;
    public GameObject ElevatorKeyText;

    [Header("Button")]
    public bool buttonPushed;

    void Start()
    {
        portalUp = false;
        portalAmim = portal.GetComponent<Animator>();
        portalAmim.enabled = false;

        elevatorGo = false;
        elevatorAnim = elevator.GetComponent<Animator>();
        elevatorAnim.enabled = false;
    }

    /// <summary>
    /// Make the portal go up
    /// </summary>
    public void DoPortal()
    {
        if (!portalUp)
        {
            portalAmim.enabled = true;
            portalUp = true;
        }
    }

    /// <summary>
    /// Make the elevator elevate
    /// </summary>
    public void DoElevator()
    {
        if (playerInvent.HasItem(orangeKey))
        {
            elevatorAnim.enabled = true;
            elevatorGo = true;
        }
        else
        {
            Debug.Log("You need to find the orange key");
        }
    }

    /// <summary>
    /// Push the button
    /// </summary>
    /// <param name="button"></param>
    public void ButtonPush(GameObject button)
    {
        if (!buttonPushed)
        {
            buttonPushed = true;
            button.GetComponent<Animator>().enabled = true;
        }
    }
}
