﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Animation event to keep the portal in the up position
/// </summary>
public class PortalAnimEvent : MonoBehaviour
{
    public Vector3 portalUpPos;

    public void PortalAnimEnd()
    {
        gameObject.GetComponent<Animator>().enabled = false;
        transform.position = portalUpPos;
    }
}
