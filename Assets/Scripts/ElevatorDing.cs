﻿using UnityEngine;

public class ElevatorDing : MonoBehaviour
{
    public AudioSource source;
    [SerializeField]
    AudioClip clip;

    // Use this for initialization
    void Start()
    {
        clip = source.clip;
    }

    public void DoDing()
    {
        source.PlayOneShot(clip);
    }
}
