﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeManager : MonoBehaviour
{
    //Array of all the cubes in the game
    public GameObject[] cubes;
    //How long to wait until they respawn
    public float spawnWaitTime;

    //Start the respawn wait
    void Start()
    {
        StartCoroutine(RespawnCubesWait());
    }

    /// <summary>
    /// Wait until is it alright to respawn the cubes then do it
    /// Restart the wait
    /// </summary>
    /// <returns></returns>
    IEnumerator RespawnCubesWait()
    {
        yield return new WaitForSeconds(spawnWaitTime);
        RespawnCubes();
        StartCoroutine(RespawnCubesWait());
    }

    /// <summary>
    /// Run through all the cubes and if they are disabled, enable them (respawn them)
    /// </summary>
    void RespawnCubes()
    {
        for(int i = 0; i < cubes.Length; i++)
        {
            if (!cubes[i].activeInHierarchy)
            {
                cubes[i].SetActive(true);
            }
        }
    }
}
