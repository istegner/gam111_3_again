﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using TMPro;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


public class MainMenuBtnManager : MonoBehaviour
{
    //The main heading
    public TMP_Text gameName;
    //The games version number
    public TMP_Text versionNumber;
    //The resolutions the player can choose from (differs from computer to computer)
    Resolution[] resolutions;
    //The dropdown for the resolutions
    public TMP_Dropdown resolutionDropdown;
    //The dropdown for the quality options
    public TMP_Dropdown qualityDropdown;
    //The toggle for fullscreen
    public Toggle fullscreenToggle;
    //The toggle to toggle showing fps
    public Toggle showFpsToggle;
    //The advanced fps
    public Toggle showAdvancedFPSToggle;
    public GameObject advancedFPS;

    [Header("PlayerOptions")]
    //The players horizontal sensitivity
    public Slider horizontalSlider;
    //The players vertical sensitivity
    public Slider verticalSlider;
    //The text elements to display the player sensitivity as a number
    public TMP_Text horiValue;
    public TMP_Text vertValue;
    //Toggle for the player head bob
    public Toggle headBobToggle;
    //Have the game settings been loaded from file
    public bool hasLoaded = false;

    public GameObject player;

    //The canvas for the fade effect
    public GameObject fadeEffectCanvas;
    //Has the game been faded already
    bool hasFaded = false;
    
    void OnEnable()
    {
        //Show the games name as the header
        gameName.text = Application.productName;
        //Display the games version in the corner
        versionNumber.text = "v: " + Application.version;
        //Get the screens available resolutions
        resolutions = Screen.resolutions;
        //Clear the resolution dropdown
        resolutionDropdown.ClearOptions();
        //Create a list of resolutions and put then into the dropdown
        List<string> options = new List<string>();
        int currentResIndex = 0;
        for(int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            if(resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResIndex = i;
            }
        }
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResIndex;
        resolutionDropdown.RefreshShownValue();
        //Load the game settings from file and apply them
        LoadSettings();
        //qualityDropdown.RefreshShownValue();
        horizontalSlider.value = player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.XSensitivity;
        verticalSlider.value = player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.YSensitivity;
        headBobToggle.isOn = player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_UseHeadBob;
        fullscreenToggle.isOn = Screen.fullScreen;
        showFpsToggle.isOn = this.GetComponent<FPSCounter>().show;
    }

    void Update()
    {
        ////While the game settings haven't been loaded, show the loading screen
        //while (!hasLoaded)
        //{
        //    this.GetComponent<LoadingScreenController>().loadingScreenCanvas.SetActive(true);
        //    this.GetComponent<LoadingScreenController>().progressBar.value = 0.9f;
        //}
        //Show the players sensitivity to 2 decimal places
        horiValue.text = horizontalSlider.value.ToString("F2");
        vertValue.text = verticalSlider.value.ToString("F2");
        //If the scene has faded in, disable the fade canvas (was preventing button presses when built)
        if(this.GetComponent<FadeManager>().black.color.a == 0 && !hasFaded)
        {
            fadeEffectCanvas.SetActive(false);
            hasFaded = true;
        }
    }

    /// <summary>
    /// Load the first level
    /// </summary>
    /// <param name="index"></param>
    public void StartBtn(int index)
    {
        fadeEffectCanvas.SetActive(true);
        this.GetComponent<FadeManager>().StartFading(index);
    }

    /// <summary>
    /// Quit the game
    /// </summary>
    public void ExitBtn()
    {
        Application.Quit();
        Debug.Log("Quit");
    }

    /// <summary>
    /// Set the game quality based of the dropdown value
    /// </summary>
    /// <param name="qualityIndex"></param>
    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    /// <summary>
    /// Enable/disable fullscreen mode
    /// </summary>
    /// <param name="isFullscreen"></param>
    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }

    public void ShowFPS(bool fpsToggle)
    {
        this.GetComponent<FPSCounter>().show = fpsToggle;
    }

    public void ShowAdvancedFPS(bool advancedFPSToggle)
    {
        advancedFPS.SetActive(advancedFPSToggle);
    }

    /// <summary>
    /// Set the resolution of the game window based on the dropdown
    /// </summary>
    /// <param name="resolutionIndex"></param>
    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    /// <summary>
    /// Enable/disable headbob
    /// </summary>
    /// <param name="headBob"></param>
    public void HeadBob(bool headBob)
    {
        player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_UseHeadBob = headBob;
    }

    /// <summary>
    /// Set the vertical sensitivity
    /// </summary>
    /// <param name="ySensitivity"></param>
    public void SetVertSensitivity(float ySensitivity)
    {
        player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.YSensitivity = ySensitivity;
    }

    /// <summary>
    /// Set the horizontal sensitivity
    /// </summary>
    /// <param name="xSensitivity"></param>
    public void SetHoriSensitivity(float xSensitivity)
    {
        player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.XSensitivity = xSensitivity;
    }

    /// <summary>
    /// Save the game settings to file
    /// </summary>
    public void SaveSettings()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/GameSettings.dat");

        GameSettings data = new GameSettings();
        data.resolution = resolutionDropdown.value;
        data.fullscreen = Screen.fullScreen;
        data.showFPS = this.GetComponent<FPSCounter>().show;
        data.showAdvancedFPS = advancedFPS.activeInHierarchy;
        data.graphics = QualitySettings.GetQualityLevel();
        data.headBob = player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_UseHeadBob;
        data.horiSensitivity = player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.XSensitivity;
        data.vertSensitivity = player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.YSensitivity;

        bf.Serialize(file, data);
        file.Close();
    }

    /// <summary>
    /// Load the game settings from file
    /// </summary>
    public void LoadSettings()
    {
        Debug.Log("Loading settings...");
        hasLoaded = false;
        if (File.Exists(Application.persistentDataPath + "/GameSettings.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/GameSettings.dat", FileMode.Open);
            GameSettings data = (GameSettings)bf.Deserialize(file);
            file.Close();

            resolutionDropdown.value = data.resolution;
            Screen.fullScreen = data.fullscreen;
            this.GetComponent<FPSCounter>().show = data.showFPS;
            advancedFPS.SetActive(data.showAdvancedFPS);
            
            QualitySettings.SetQualityLevel(data.graphics);
            player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_UseHeadBob = data.headBob ;
            player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.XSensitivity = data.horiSensitivity;
            player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.YSensitivity = data.vertSensitivity;
        }
        hasLoaded = true;
        Debug.Log("Done loading settings.");
    }
}

/// <summary>
/// Neat reference to the games settings for easy saving/loading
/// </summary>
[System.Serializable]
class GameSettings
{
    public int resolution;
    public bool fullscreen;
    public int graphics;
    public bool showFPS;
    public bool showAdvancedFPS;

    public bool headBob;
    public float horiSensitivity;
    public float vertSensitivity;
}
