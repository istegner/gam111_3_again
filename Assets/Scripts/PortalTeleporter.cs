﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTeleporter : MonoBehaviour
{
    public Transform player;
    //Reference to the other portal
    public Transform receiver;
    //If the player should be teleported
    private bool playerIsOverlapping = false;

    void Update()
    {
        //If the player has entered the portal
        if (playerIsOverlapping)
        {
            //The players position relative to the portal
            Vector3 portalToPlayer = player.position - transform.position;
            //All the neccessary calculations to teleport the player to the right transform for a smooth experience
            float dotProduct = Vector3.Dot(transform.up, portalToPlayer);
            if (dotProduct < 0)
            {
                float rotationDiff = Quaternion.Angle(transform.rotation, receiver.rotation);
                rotationDiff += 180;
                player.Rotate(Vector3.up, rotationDiff);

                Vector3 positionOffset = Quaternion.Euler(0f, rotationDiff, 0f) * portalToPlayer;
                player.position = receiver.position + positionOffset;

                playerIsOverlapping = false;
            }
        }
    }

    /// <summary>
    /// When the player enters the portal, allow then to teleport
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerIsOverlapping = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            playerIsOverlapping = false;
        }
    }
}
