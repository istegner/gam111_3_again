﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The players invent
/// </summary>
[CreateAssetMenu()]
public class InventorySO : ScriptableObject
{
    public List<ItemSO> heldItems = new List<ItemSO>();

    /// <summary>
    /// Check if the player has an item
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public bool HasItem(ItemSO item)
    {
        return heldItems.Contains(item);
    }

    /// <summary>
    /// Add an item to the players invent
    /// </summary>
    /// <param name="item"></param>
    public void AddItem(ItemSO item)
    {
        heldItems.Add(item);
    }

    /// <summary>
    /// Remove an item from the players invent
    /// </summary>
    /// <param name="item"></param>
    public void RemoveItem(ItemSO item)
    {
        heldItems.Remove(item);
    }

    /// <summary>
    /// Clear the players invent
    /// </summary>
    public void Clear()
    {
        heldItems.Clear();
    }
}
