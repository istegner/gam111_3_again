Post Effects
	Make use of the built-in post effects available in the engine
State-based character
	3 states or actions
		Might be actions based on the part of the puzzle the player is solving
		Might be roaming, pursuing, fleeing, etc
'Puzzle'
	Multiple steps to complete
	Can be simple variations on Lock and Key puzzles
	Include solution in accompanying documentation


You need to find the orange key in anim controller
Move settings save/load into its own script

Fix bullet hit particle
Fix cubes on elevator fucking up
Fix graphics level dropdown starting on Very Low even if it isnt
Fix Show FPS toggle defaulting to on in level 1 options menu
Fix curser not turning off and locking when using escape key to level main menu